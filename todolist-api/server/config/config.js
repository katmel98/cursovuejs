var env = process.env.NODE_ENV || 'development';
console.log('env ******>>', env);

if(env === 'development' || env === 'mongo_atlas') {
    var config = require('./config.json');
    var envConfig = config[env];
    console.log(envConfig);
    Object.keys(envConfig).forEach((key) => {
        process.env[key] = envConfig[key];
    });
}