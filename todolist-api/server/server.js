require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const _ = require('lodash');

var {Todo} = require('./models/todo');
var {User} = require('./models/user');

var {mongoose} = require('./db/mongoose');

var app = express();
const port = process.env.PORT || 4000;
app.use(bodyParser.json());
app.use(cors());

// Obtiene toda la lista de TODOS desde la base de datos
app.get('/todos', (req, res) => {
    Todo.find({})
    .then((todos) => {
        res.send({todos})
    }, (e) => {
        res.status(400).send(e);
    })
});

// Crea un nuevo TODO en la BBDD
app.post('/todos', (req, res) => {
    var todo = new Todo({
        text: req.body.text
    });
    todo.save().then((doc) => {
        res.status(200).send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
});

// Modifica un TODO
app.patch('/todos/:id', (req, res) => {
    var id = req.params.id;
    var body = _.pick(req.body, ['text', 'completed']);

    if(_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }
    Todo.findOneAndUpdate(
        {
        _id: id
        },
        {
            $set: body
        },{
            new: true
        }
    ).then((todo) => {
        if(!todo) {
            return res.status(404).send();
        }
        res.send({todo});
    }).catch((e) => {
        res.status(400).send();
    });
});

// Elimina un TODO
app.delete('/todos/:id', async (req, res) => {
    const id = req.params.id;

    try {
        const todo = await Todo.findOneAndRemove({
            _id: id
        });
        if(!todo){
            return res.status(404).send('ID not found');
        }
        res.send({todo});
    } catch (e) {
        res.status(400).send(e);
    }
})

// Fetch users list: GET
app.get('/users', (req, res) => {
    console.log("Estoy en el post de listar elementos ...");
    User.find({})
    .then((users) => {
        res.send({users})
    }, (e) => {
        res.status(400).send(e);
    })
});

// Create = POST
app.post('/users', async (req, res) => {
    console.log("Estoy en el post de creación ...");
    try {
        const body = _.pick(req.body, ['email', 'password', 'name', 'lastname', 'surname']);
        const user = new User(body);
        await user.save();
        // const token = await user.generateAuthToken();  
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }    
});

// Update = PATCH
app.patch('/users/:id', async (req, res) => {
    console.log("Estoy en el post de modificación ...");
    var id = req.params.id;    
    const body = _.pick(req.body, ['name', 'lastname', 'surname']);
    User.findOneAndUpdate(
        {
        _id: id
        },
        {
            $set: body
        },{
            new: true
        }
    ).then((user) => {
        if(!user) {
            return res.status(404).send();
        }
        res.send({user});
    }).catch((e) => {
        res.status(400).send();
    });
    // const token = await user.generateAuthToken();  
});

// Delete = DELETE
app.delete('/users/:id', async (req, res) => {
    const id = req.params.id;

    try {
        const user = await User.findOneAndRemove({
            _id: id
        });
        if(!user){
            return res.status(404).send('ID not found');
        }
        res.send({user});
    } catch (e) {
        res.status(400).send(e);
    }
})


app.get('/users/me', (req, res) => {
    res.send(req.user)
});

app.post('/users/login', async (req, res) => {
    console.log("Estoy en el login ...");
    try {
        var body = _.pick(req.body, ['email', 'password']);
        const user = await User.findByCredentials(body.email, body.password);
        console.log(user);
        if (!user) {
            res.status(404).send('Resource NOT FOUND');
        }
        // const token = await user.generateAuthToken();
        res.send(user);
    } catch (e) {
        console.log("EL ERROR");
        if(e==404){
            res.status(404).send('Resource NOT FOUND')    
        }
        res.status(400).send()
    }
});

app.delete('/users/me/token', async (req, res) => {
    try {
        await req.user.removeToken(req.token);
        res.status(200).send();
    } catch (e) {
        res.status(400).send();
    }
});

app.listen(port, () => {
    console.log(`Iniciado el Express en el puerto ${port}`);
});

module.exports = {app};