import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store(
    {
        state: {
            todos: []
        },
        getters: {
            TODOS: state => {
                return state.todos;
            }
        },
        mutations: {
            SET_TODO: (state, data) => {
                state.todos = data;
            }
        },
        actions: {
            GET_TODOS : async (context) => {
                let {data} = await axios.get('http://localhost:4000/todos')
                data.todos.forEach(element => {
                    element.editing = false;
                });
                context.commit('SET_TODO', data.todos);
             },
             ADD_TODO : async (context, todo) => {
                await axios.post('http://localhost:4000/todos', todo);
                context.dispatch('GET_TODOS');
             },
             UPDATE_TODO : async (context, todo) => {
                let id = todo._id;
                await axios.patch('http://localhost:4000/todos/' + id, todo);
             },
             DELETE_TODO: async (context, id) => {
                await axios.delete('http://localhost:4000/todos/' + id);
                context.dispatch('GET_TODOS');
             }
        }
    }
)