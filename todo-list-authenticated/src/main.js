import Vue from 'vue';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App.vue';
import store from './store';
import {router} from './router'

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');

// http://jasonwatmore.com/post/2018/07/14/vue-vuex-user-registration-and-login-tutorial-example