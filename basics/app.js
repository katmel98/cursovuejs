var express = require('express');
var path = require('path');
var app = express();

const publicPath = path.join(__dirname, '/public');
app.use(express.static(publicPath));

app.get('/form', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/template/template.html'));
});

app.get('/intro', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/cover/index.html'));
});

app.get('/components', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/basic_components/components.html'));
});

app.get('/separated', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/components/src/index.html'));
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});